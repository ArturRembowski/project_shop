<?php session_start(); include_once "layout/scripts-php.php"; ?>
<!DOCTYPE html>
<html lang="pl">
<head>
    <?php include_once "layout/head.php"; ?>
</head>
<body>
    <?php include_once "layout/header.php"; ?>

    <div id="main-section">
        <?php include_once "layout/mainsection-leftbar.php"; ?>

        <div id="content">
            <form method="post">
                <div class="falf-form-label">Pay date</div>
                <input id="admin-paymentDate" type="text" class="date" name="paymentDate" />
                <div class="clear"></div>

                <div class="falf-form-label">Send date</div>
                <input id="admin-sentDate" class="date" type="text" name="sentDate" />
                <div class="clear"></div>

                <input type="submit" class="falf-button submit" style="margin-left: 240px;" value="Zatwierdz" />
            </form>
        </div>

        <?php include_once "layout/mainsection-rightbar.php"; ?>

    </div>

    <?php include_once "layout/footer.php"; ?>

    <?php include_once "layout/scripts-js.php"; ?>
</body>
</html>
<?php session_start(); include_once "layout/scripts-php.php"; ?>
<!DOCTYPE html>
<html lang="pl">
<head>
    <?php include_once "layout/head.php"; ?>
</head>
<body>
    <?php include_once "layout/header.php"; ?>

    <div id="main-section">
        <?php include_once "layout/mainsection-leftbar.php"; ?>

        <div id="content">
            <h1>Wallet</h1>
        </div>

        <?php include_once "layout/mainsection-rightbar.php"; ?>

    </div>

    <?php include_once "layout/footer.php"; ?>

    <?php include_once "layout/scripts-js.php"; ?>
</body>
</html>

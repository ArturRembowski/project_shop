﻿HTMLBodyElement.onload = clockStart();

$("#head-logo").click(function () { ReplaceContentWithGenres();});
$("#head-title").click(function () { ReplaceContentWithGenres();});
$("#head-search").click(function () { ReplaceContentWithSearch();});
$("#head-contact").click(function () { ReplaceContentWithContact();});
$("#head-shop").click(function () { ReplaceContentWithShop(); });
$("#admin-category").click(function () { AdminCategory(); });
$("#admin-product").click(function () { AdminProduct(); });
$("#admin-users").click(function () { AdminUsers(); });
$("#admin-orders").click(function () { AdminOrders(); });
$("#user-details").click(function () { UserDetails(); });
$("#user-basket").click(function () { UserBasket(); });
$("#user-orders").click(function () { UserOrders(); });

$("#cookie-button").click( function()
{
    $.ajax({
        type: "POST",
        url: "cookie-click.php",
        success: function (response) {
            $("#cookies").slideUp(500);
        },    
    });
});
$("#js-check").remove();

function post(path, parameters) {

    var form = $('<form></form>');

    form.attr("method", "post");
    form.attr("action", path);

    $.each(parameters, function (key, value) {
        if (typeof value == 'object' || typeof value == 'array') {
            $.each(value, function (subkey, subvalue) {
                var field = $('<input />');
                field.attr("type", "hidden");
                field.attr("name", key + '[]');
                field.attr("value", subvalue);
                form.append(field);
            });
        } else {
            var field = $('<input />');
            field.attr("type", "hidden");
            field.attr("name", key);
            field.attr("value", value);
            form.append(field);
        }
    });
    $(document.body).append(form);
    form.submit();
}   

function ContentGenreClick(element) {
    post('genre.php', { name: element.id.slice(8), siema: "elo" });
}

function LeftbarGenreClick(element) {
    post('genre.php', { name: element.id.slice(8) });
}

function clockStart() {
    try
    {
        var today = new Date();
        if (today == null) {
            throw Error("nie działa pobranie daty");
        }
    }
    catch (e)
    {
        alert(e.message);
    }
    var day = today.getDate();
    var month = today.getMonth() + 1;
    var year = today.getFullYear();
    var hour = today.getHours();
    if (hour < 10) hour = "0" + hour;

    var minute = today.getMinutes();
    if (minute < 10) minute = "0" + minute;

    var second = today.getSeconds();
    if (second < 10) second = "0" + second;

    document.getElementById("clock").innerHTML =
     day + "/" + month + "/" + year + " | " + hour + ":" + minute + ":" + second;

    setTimeout("clockStart()", 1000);
}


function ReplaceContentWithGenres() {
    $.ajax({
        type: "GET",
        url: "genres.php",
        success: function (response) {
            $("#content").replaceWith(response);
        }
    });
}

function ReplaceContentWithSearch() {
    $.ajax({
        type: "GET",
        url: "search.php",
        success: function (response) {
            $("#content").replaceWith(response);
        }
    });
}

function ReplaceContentWithContact() {
    $.ajax({
        type: "GET",
        url: "contact.php",
        success: function (response) {
            $("#content").replaceWith(response);
        }
    });
}

function ReplaceContentWithAccount() {
    $.ajax({
        type: "GET",
        url: "account.php",
        success: function (response) {
            $("#content").replaceWith(response);
        }
    });
}

function ReplaceContentWithShop() {
    $.ajax({
        type: "GET",
        url: "shop.php",
        success: function (response) {
            $("#content").replaceWith(response);
        }
    });
}

function ReplaceContentWithAccountLogged() {
    $.ajax({
        type: "GET",
        url: "login.php",
        success: function (response) {
            $("#content").replaceWith(response);
        }
    });
}

function AdminCategory() {
    $.ajax({
        type: "GET",
        url: "account-admin-category.php",
        success: function (response) {
            $("#admin-choice").replaceWith(response);
        }
    });
}

function AdminProduct() {
    $.ajax({
        type: "GET",
        url: "account-admin-product-form.php",
        success: function (response) {
            $("#admin-choice").replaceWith(response);
        }
    });
}

function AdminUsers() {
    $.ajax({
        type: "GET",
        url: "account-admin-users.php",
        success: function (response) {
            $("#admin-choice").replaceWith(response);
        }
    });
}

function AdminOrders() {
    $.ajax({
        type: "GET",
        url: "account-admin-orders.php",
        success: function (response) {
            $("#admin-choice").replaceWith(response);
        }
    });
}

$(function () {
    $("#admin-sentDate").datepicker({ dateFormat: 'yy-mm-dd' });
});

$(function () {
    $("#admin-paymentDate").datepicker({ dateFormat: 'yy-mm-dd' });
});

function UserDetails() {
    $.ajax({
        type: "GET",
        url: "account-user-details.php",
        success: function (response) {
            $("#user-choice").replaceWith(response);
        }
    });
}

function UserBasket() {
    $.ajax({
        type: "GET",
        url: "account-user-basket.php",
        success: function (response) {
            $("#user-choice").replaceWith(response);
        }
    });
}

function UserOrders() {
    $.ajax({
        type: "GET",
        url: "account-user-orders.php",
        success: function (response) {
            $("#user-choice").replaceWith(response);
        }
    });
}
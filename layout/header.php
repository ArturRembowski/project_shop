<?php
if(!isset($_COOKIE['Cookies']))
{
    echo '<div id="cookies">
     <div class="header-error-el"></div>
    <div id="cookie-text" class="header-error-el">This site is using cookies files.</div>
    <div class="header-error-el"><div id="cookie-button" class="falf-button big">Agreed</div></div>
    </div>';
}

?>
<div id="js-check" class="header-error-el">Please do not disable javascript support.</div>
<div style="clear:both;"></div>

<div id="header">

    <a href="index.php">
        <div class="head-el" id="head-logo">
            <img src="img/logo.jpg" height="60" width="225" />
        </div>
    </a>
    <a href="index.php">
        <div class="head-el" id="head-title">
            <h1>Shop</h1>
        </div>
    </a>

    <a href="search.php">
        <div class="head-el" id="head-search">
            <i class="icon-search"></i>
            <br />
            Searcher
        </div>
    </a>

    <a href="contact.php">
        <div class="head-el" id="head-contact">
            <i class="icon-mail"></i>
            <br />
            Contact
        </div>
    </a>
    <a href="account.php">
        <div class="head-el" id="head-account">
            <i class="icon-home"></i>
            <br />
           My account
        </div>
    </a>
    <a href="shop.php">
        <div class="head-el" id="head-shop">
            <!--<i class="icon-basket"></i>
            <br />
            do kasy-->
        </div>
    </a>
</div>

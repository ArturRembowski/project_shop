
    <div id="content">
            <?php
            require "dbconnect.php";      
            try
            {
                $db_connect = @new mysqli($host, $db_user, $db_password, $db_name);
                if ($db_connect->connect_errno!=0)
                { 
                    throw new Exception($db_connect->connect_errno);
                }
                else
                {
                    $db_query = mysqli_query($db_connect, "select * from genres");
                    while ($db_results = mysqli_fetch_array($db_query)) 
                    { ?>
                        <div id="content-<?php echo $db_results['GenreName']; ?>" name="<?php echo $db_results['GenreName']; ?>"  class="genre"
                             onclick="ContentGenreClick(this);">
                            <img class="bottom" src="img/<?php echo $db_results['GenreName']; ?>2.jpg" />
                            <img class="top" src="img/<?php echo $db_results['GenreName']; ?>.jpg" />
                        </div> 
                    
              <?php }
                    $db_connect->close();
                }      
            }
            catch (Exception $exc)
            {
                echo "Database error. Please contact administrator " + $exc->getMessage();
            }               
            ?> 
    </div>

<?php session_start(); include_once "layout/scripts-php.php"; ?>
<!DOCTYPE html>
<html lang="pl">
<head>
    <?php include_once "layout/head.php"; ?>
</head>
<body>
    <?php include_once "layout/header.php"; ?>

    <div id="main-section">
        <?php include_once "layout/mainsection-leftbar.php"; ?>

        <div id="content">
            <form method="post" action="account-admin-product-form-submit.php">

                <input type="hidden" value="<?php echo $_POST['categoryAmount']; ?>" name="categoryAmount"/>

                <input type="hidden" value="<?php echo $_POST['bandAmount']; ?>" name="bandAmount"/>

                <div class="falf-form-label">Name</div>
                <input type="text" name="productName" />
                <div class="clear"></div>

                <?php
/*                for ($i = 0; $i < $_POST['categoryAmount']; $i++)
                { */?><!--

                <div class="falf-form-label">Category</div>
                <input type="text" name="productCategory<?php /*echo $i; */?>" />
                <div class="clear"></div>
                <?php
/*                  }
                */?>

                <?php
/*                for ($i = 0; $i < $_POST['bandAmount']; $i++)
                 { */?>
                <div class="falf-form-label">Team</div>
                <input type="text" name="productBand<?php /*echo $i; */?>" />
                <div class="clear"></div>
                --><?php
/*                  }
                */?>

                <div class="falf-form-label">Price</div>
                <input type="number" min="1" placeholder="PLN" name="productPrice" />
                <div class="clear"></div>

                <div class="falf-form-label">Amount</div>
                <input type="number" min="1" name="productAmount" />
                <div class="clear"></div>

                <div class="falf-form-label">Make date</div>
                <input type="number" min="1500" name="productYear" />
                <div class="clear"></div>

                <div class="falf-form-label">Description</div>
                <input type="text" name="productDescription" />
                <div class="clear"></div>


                <input type="submit" class="falf-button submit" style="margin-left: 240px;" value="Apply" onsubmit="AdminProduct();" />
            </form>
        </div>

        <?php include_once "layout/mainsection-rightbar.php"; ?>

    </div>

    <?php include_once "layout/footer.php"; ?>

    <?php include_once "layout/scripts-js.php"; ?>
</body>
</html>


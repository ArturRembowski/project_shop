<?php
    session_start();

    if(!isset($_COOKIE['user'])) 
    { 
        header("Location: account.php");
        exit();
    }


    session_unset();
    session_destroy();
    setcookie("user",'',0); unset($_COOKIE['user']);
    setcookie("session",'',0); unset($_COOKIE['session']);
    header("Location: account.php");

?>
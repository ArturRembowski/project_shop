<?php session_start(); include_once "layout/scripts-php.php";?>
<!DOCTYPE html>
<html lang="pl">
<head>
    <?php include_once "layout/head.php"; ?>
</head>
<body>
    <?php include_once "layout/header.php"; ?>

    <div id="main-section">
        <?php include_once "layout/mainsection-leftbar.php"; ?>


  <?php if(!isset($_COOKIE['user'])) 
        { ?>
            
        <div id="content">
               <?php if(isset($_SESSION['loginError'])){ echo $_SESSION['loginError'];} ?>
            <form method="post" id="form-login" action="account-login-submit.php">
                <div class="falf-form-title">
                    <h2>Logging</h2>
                </div>
                <div class="falf-form-label">Login </div>
                <input type="text" name="login" />
                <div class="clear"></div>

                <div class="falf-form-label">Password </div>
                <input type="password" name="password" />
                <div class="clear"></div>

                <input type="submit" class="falf-button submit" style="margin-left: 240px;" value="Log in" />

            </form>
            <hr />
            <?php if(isset($_SESSION['registerError'])){ echo $_SESSION['registerError'];} //?>
            <form method="post" id="form-register" action="account-register-submit.php">
                <div class="falf-form-title">
                    <h2>Registration</h2>
                </div>
                <div class="falf-form-label">Login </div>
                <input type="text" form="form-register" class="falf-form-label" name="login" />
                <div class="clear"></div>

                <div class="falf-form-label">Password </div>
                <input type="password" form="form-register" class="falf-form-label" name="password" />
                <div class="clear"></div>

                <div class="falf-form-label">E-Mail</div>
                <input type="text" form="form-register" class="falf-form-label" name="email" />
                <div class="clear"></div>

                <input type="submit" form="form-register" class="falf-button submit" style="margin-left: 240px;" value="Register yourself" />
            </form>

        </div>
<?php   }
            
        else 
        { ?>              
        <div id="content">
            <?php 
                 if(IsUserAdmin())
                 {
                     include "account-admin.php";
                 }
                 else
                 {
                     include "account-user.php";
                 }
            ?>
        </div>
        
 <?php }
        include_once "layout/mainsection-rightbar.php"; ?> 
        <div style="clear: both;"></div>
    </div>s

    <?php include_once "layout/footer.php"; ?>

    <?php include_once "layout/scripts-js.php"; ?>
</body>
</html>

